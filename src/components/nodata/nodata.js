import './style.css';
import ImageEmpty from '../../assets/no-data.svg'

function NoData() {
    return(
        <div className="main">
            <img src={ImageEmpty} className="image" alt="nodata"/>
            <h4>Ooops, We haven't data</h4>
        </div>
    )
}

export default NoData;