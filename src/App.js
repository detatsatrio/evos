import React,{ useEffect, useState } from 'react';
import { GetStarships, SearchStarships } from './api/rest';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './App.css';
import NoData from './components/nodata/nodata'
import { DataScroller } from 'primereact/datascroller';
import { ProgressSpinner } from 'primereact/progressspinner';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';

function App(props) {
  const [starships, setStarships] = useState()
  const [searchKey, setSearchKey] = useState()
  const [isLoading, setIsLoading] = useState(true)
  const [isEmpty, setEmpty] = useState(false)

  async function getDataShip(){
    try {
      const ships = await GetStarships()

      if(ships === undefined) {
        setEmpty(true)
        return;
      }

      if(ships.results.length > 0){
        setStarships(ships.results)
      }

      setIsLoading(false)
    } catch (error) {
      setEmpty(true)
    }
  }

  async function searchShip(){
    try{

      const ships = await SearchStarships(searchKey)

      if(ships !== undefined){
        setStarships(ships.results)
      }
      
    } catch (error) {
      setEmpty(true)
    }
  }

  useEffect(()=>{
    getDataShip()
  },[])

  const shipTemplate = (data) => {
    return (
      <div className="product-item">
          <div className="product-detail">
              <div className="product-name">{data.name}</div>
              <div className="product-description">{data.model}</div>
          </div>
          <Button type="button" label="Details"  />
      </div>
    );
  }

  const SearchBox = () => {
    return(
      <div className="p-grid p-fluid starship-search">
          <div className="p-col-12 p-md-4">
              <div className="p-inputgroup">
                  <Button type="button" label="Search" onClick={searchShip}/>
                  <InputText id="block" keyfilter={/^[^<>*!]+$/} value={searchKey} onChange={(e)=>setSearchKey(e.target.value)} placeholder="Search starship"/>
              </div>
          </div>
      </div>
    )
  }

  return (
    <div className="datascroller-demo">

      {isLoading && (
        <div className="card loader">
          <ProgressSpinner/>
          <h2>Mohon menunggu...</h2>
        </div>
      )}

      {(!isLoading && isEmpty) && (
        <div className="card loader">
          <NoData />
        </div>
      )}

      {(!isEmpty && !isLoading) && (
        <div className="card">
          <SearchBox />
          <DataScroller value={starships} itemTemplate={shipTemplate} rows={5} inline scrollHeight="300px" header="Scroll Down to Load More" />
        </div>
      )}
    </div>
  );
}

export default App;
