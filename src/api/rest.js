import axios from 'axios';
import {STARSHIPS} from './endpoint'

export function GetStarships(){
    return new Promise(async(resolve, reject)=>{
        axios.get(STARSHIPS)
        .then(function (response) {
            resolve(response.data)
        })
        .catch(function (error) {
            reject(error.message)
        })
    })
}

export function SearchStarships(keyword){
    return new Promise(async(resolve, reject)=>{
        axios.get(STARSHIPS+"/?search="+keyword)
        .then(function (response) {
            // handle success
            resolve(response.data)
        })
        .catch(function (error) {
            // handle error
            reject(error.message)
        })
    })
}