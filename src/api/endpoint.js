const API_URL = "https://swapi.dev/api"

export const STARSHIPS = API_URL+"/starships"
export const STARSHIPS_SCHEMA = STARSHIPS+"/schema"
