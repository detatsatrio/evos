import { render, screen } from '@testing-library/react';
import App from './App';

test('renders link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Scroll Down to Load More/i);
  expect(linkElement).toBeInTheDocument();
});
